import 'package:devhours_2020_flutter/settings_page.dart';

import 'add_comment.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  List<String> _comments = ["Comment 1", "Comment 2", "Comment 3"];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(brightness: Brightness.light, primaryColor: Colors.red),
        title: "Devhours",
        home: Builder(builder: (context) => _buildDashboard(context)));
  }

  Widget _buildDashboard(BuildContext context) {
    if (MediaQuery.of(context).orientation == Orientation.portrait)
      return Scaffold(
          appBar: AppBar(
            title: Text("Devhours Flutter"),
          ),
          endDrawer: Padding(
            padding: EdgeInsets.only(left: 40),
            child: Container(
              color: Colors.white,
              width: 200,
              child: Column(
                children: [Image.asset("assets/placeholder.png")],
              ),
            ),
          ),
          floatingActionButton: FloatingActionButton(
            child: Icon(Icons.add_comment),
            onPressed: () async {
              AddCommentDialog dialog = AddCommentDialog();

              String toAdd = await dialog.showAddCommentDialog(context);
              if (toAdd != null) {
                setState(() {
                  _comments.add(toAdd);
                });
              }
            },
          ),
          body: _buildBody(context));
    else
      return Container(
        color: Colors.blue,
      );
  }

  Widget _buildBody(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Column(children: [
        Container(
          width: MediaQuery.of(context).size.width,
          child: FadeInImage.assetNetwork(
              fit: BoxFit.cover,
              height: 200,
              placeholder: "assets/placeholder.png",
              image:
                  "https://images.unsplash.com/photo-1607714724990-8f5495b75883?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80"),
        ),
        _buildButton(context),
        _buildList(context)
      ]),
    );
  }

  Widget _buildButton(BuildContext context) {
    return RaisedButton(
      onPressed: () async {
        int result = await Navigator.push(context, MaterialPageRoute(builder: (context) {
          return SettingsPage();
        }));

        print("Result: $result");
      },
      color: Colors.redAccent,
      child: Text(
        "Settings",
        style: TextStyle(color: Colors.white),
      ),
    );
  }

  Widget _buildList(BuildContext context) {
    return Expanded(
      child: ListView.builder(
          itemCount: _comments.length,
          itemBuilder: (context, index) {
            return ListTile(
              title: Text(
                _comments[index],
                style: TextStyle(color: Colors.green, fontSize: 24, fontWeight: FontWeight.bold),
              ),
              trailing: GestureDetector(
                onTap: () {
                  setState(() {
                    _comments.remove(_comments[index]);
                  });
                },
                child: Icon(
                  Icons.remove_circle_outline,
                  color: Colors.redAccent,
                ),
              ),
            );
          }),
    );
  }
}
