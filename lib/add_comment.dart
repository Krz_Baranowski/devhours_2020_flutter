import 'package:flutter/material.dart';

class AddCommentDialog {
  TextEditingController _controller = new TextEditingController();

  Future<String> showAddCommentDialog(BuildContext context) async {
    String commentToAdd = await showDialog<String>(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("Enter a comment"),
            content: _buildContent(context),
            actions: [
              TextButton(
                  onPressed: () {
                    String toAdd = _controller.text;
                    if (toAdd.isNotEmpty) Navigator.pop(context, toAdd);
                  },
                  child: Text("Add"))
            ],
          );
        });

    return commentToAdd;
  }

  Widget _buildContent(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Icon(
            Icons.add_comment,
            size: 64,
            color: Colors.orangeAccent,
          ),
          TextField(
            controller: _controller,
          )
        ],
      ),
    );
  }
}
